# Tor specification repository

This is the source code of the Tor specification documents.

**To read the Tor Specifications online**,
please go to the canonical site <https://spec.torproject.org/>.

Overview of the source code layout, and build instructions,
are in `spec/back-matter.md`.

The canonical source repository is on the Tor Project's gitab, at
<https://gitlab.torproject.org/tpo/core/torspec/>
